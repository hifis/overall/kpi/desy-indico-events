# DESY Indico - Events

Usage numbers for DESY Indico (HIFIS Events Service).

According to <https://events.hifis.net/category/0/statistics.json>

Data reported:
* files (all files stored)
* total_contributions
* total_events
* users (registered users with login)

Additional data (updated irregularly from <https://events.hifis.net/admin/users/>)
* deleted_users
